# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]
### [Changed]
- SignalGroup::repetitions is set to 1 by the constructor
### [Fixed]
- Allow to read files that have messages without blank line as separator

## [1.0.1] - 2016-05-20
### Added
- Compiler hardening flags enabled

## [1.0.0] - 2016-05-20
### Added
- Initial version
